terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "api_token" {
  type        = string
  description = "DigitalOcean API token"
  default     = ""
  sensitive   = true
}

variable "ssh_priv_key" {
  type        = string
  description = "Location of your private ssh key."
  default     = "~/.ssh/id_rsa"
}

provider "digitalocean" {
  token = var.api_token
}

data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}

resource "digitalocean_droplet" "teamserver" {
  count              = 3
  image              = "ubuntu-20-04-x64"
  name               = "cs-teamserver-${count.index}"
  region             = "nyc3"
  size               = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    "teamserver",
    "development",
    "deployment"
  ]

  connection {
    host        = self.ipv4_address
    user        = "root"
    type        = "ssh"
    private_key = file(var.ssh_priv_key)
    timeout     = "2m"
  }

  #provisioner "remote-exec" {
  #  inline = [
  #    "export PATH=$PATH:/usr/bin",
  #    "apt-get update",
  #    "apt-get install -y --no-install-recommends curl ca-certificates expect openjdk-11-jdk",
  #    "apt-get clean",
  #    "rm -rf /var/lib/apt/lists/*",
  #    "rm -rf /tmp/*",
  #    "rm -rf /var/tmp/*",
  #    "update-java-alternatives -s java-1.11.0-openjdk-amd64",
  #    "apt-get update",
  #    "apt-get install -y --no-install-recommends iproute2"
  #  ]
  #}
}

resource "local_file" "generated" {
  content = templatefile("/home/justin/Desktop/Terraform_Digital_Ocean/templates/templates.tpl",
    {
      endpoints = [for address in "${digitalocean_droplet.teamserver[*]}" : address.ipv4_address]
  })
  filename = "/home/justin/Desktop/Terraform_Digital_Ocean/inventory.yml"
  depends_on = [
    digitalocean_droplet.teamserver
  ]
}

output "digitalocean_ips" {
  value = ["${digitalocean_droplet.teamserver.*.ipv4_address}"]
}